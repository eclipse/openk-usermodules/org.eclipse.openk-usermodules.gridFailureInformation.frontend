/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component } from '@angular/core';
import { OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-expandable',
  templateUrl: './expandable.component.html',
  styleUrls: ['./expandable.component.scss'],
})
export class ExpandableComponent implements OnInit {
  @Input()
  public showBodyInitially: boolean = false;

  static count: number = 0;

  public isExpandedStatus = false;
  public collapseId: string = null;
  public showBody: string = null;

  constructor() {
    this.collapseId = 'collapse' + ExpandableComponent.count;
    ExpandableComponent.count++;
  }

  ngOnInit() {
    if (this.showBodyInitially) {
      this.showBody = 'show';
      this.isExpandedStatus = true;
    }
  }
}
