/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DateTimePickerComponent, CustomDatepickerI18n } from '@grid-failure-information-app/shared/components/date-time-picker/date-time-picker.component';
import { NgbPopoverConfig } from '@ng-bootstrap/ng-bootstrap';
import { DateTimeModel } from '@grid-failure-information-app/shared/models/date-time.model';
import { async } from '@angular/core/testing';

describe('DateTimePickerComponent', () => {
  let component: DateTimePickerComponent;
  let customDatePicker: CustomDatepickerI18n;

  let config: NgbPopoverConfig;

  beforeEach(() => {
    config = {
      autoClose: {},
      placement: {},
    } as any;

    component = new DateTimePickerComponent(config);
    customDatePicker = new CustomDatepickerI18n();
    component.dateTime = new DateTimeModel();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should call changeDateTime(event$)', () => {
    const dateTime = {
      day: 1,
      month: 3,
      year: 2020,
    };
    component.changeDateTime(dateTime);
    const newDateTime = component.dateTime;
    expect(newDateTime.day).toEqual(dateTime.day);
    expect(newDateTime.month).toEqual(dateTime.month);
    expect(newDateTime.year).toEqual(dateTime.year);
  });

  it('should call changeTime(event$)', () => {
    const dateTime = {
      hour: 1,
      minute: 1,
      second: 1,
    };

    component.changeDateTime(dateTime);
    const newDateTime = component.dateTime;
    expect(newDateTime.hour).toEqual(dateTime.hour);
    expect(newDateTime.minute).toEqual(dateTime.minute);
    expect(newDateTime.second).toEqual(dateTime.second);
  });

  it('should has providers and call getWeekdayShortName(weekday: number)', () => {
    const selectMonday = customDatePicker.getWeekdayShortName(1);
    expect(selectMonday).toEqual('Mo');
  });
  it('should has providers and call getMonthShortName(month: number)', () => {
    const selectJanuary = customDatePicker.getMonthShortName(1);
    expect(selectJanuary).toEqual('Jan');
  });
  it('should has providers and call getMonthFullName(month: number)', () => {
    const selectJanuary = customDatePicker.getMonthFullName(1);
    expect(selectJanuary).toEqual('Jan');
  });
  it('should has providers and call getDayAriaLabel(date: NgbDateStruct)', () => {
    const date = {
      year: 2001,
      month: 1,
      day: 1,
    };
    const getDate = customDatePicker.getDayAriaLabel(date);
    expect(getDate).toEqual('1-1-2001');
  });
  it('should call changeDateTime() and check if it works', () => {
    component.dateTime = null;
    const date = {
      year: 2001,
      month: 1,
      day: 1,
    };
    component.changeDateTime(date);
    expect(component.dateTime).not.toBe(null);
  });
  it('should call clearDatTime and check if it works', async(() => {
    component.clearDateTime();
    expect(component.dateTime).toBe(null);
  }));
});
