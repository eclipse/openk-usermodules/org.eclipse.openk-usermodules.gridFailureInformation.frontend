/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class Contact {
  public uuid: string = null;
  public anonymized: string = null;
  public companyId: string = null;
  public companyName: string = null;
  public companyType: string = null;
  public contactType: string = null;
  public department: string = null;
  public email: string = null;
  public firstName: string = null;
  public lastName: string = null;
  public mainAddress: string = null;
  public name: string = null;
  public note: string = null;
  public personType: string = null;
  public personTypeUuid: string = null;
  public salutationType: string = null;
  public salutationUuid: string = null;
  public searchfield: string = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }

  get contactSearchString(): string {
    return (
      (this.salutationType ? this.salutationType : '') +
      ' ' +
      this.name +
      ' ' +
      (this.personType ? this.personType : '') +
      ' ' +
      (this.department ? this.department : '') +
      ' ' +
      this.mainAddress
    );
  }
}
