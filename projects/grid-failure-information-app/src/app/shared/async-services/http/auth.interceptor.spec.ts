/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { HeaderInterceptor } from './auth.interceptor';

describe('HeaderInterceptor', () => {
  let interceptor = new HeaderInterceptor();
  beforeEach(() => {
    localStorage.setItem('token', 'my_token');
  });

  afterEach(() => {
    localStorage.removeItem('token');
  });

  it('should call baseAdapter without result if response is not 200', () => {
    const request: any = { clone: () => {} };
    const next: any = { handle: () => {} };
    const spy = spyOn(next, 'handle');
    interceptor.intercept(request, next);
    expect(spy).toHaveBeenCalled();
  });
});
