/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';
import { DistributionGroupApiClient } from '@grid-failure-information-app/pages/distribution-group/distribution-group-api-client';
import { catchError, map, switchMap, exhaustMap } from 'rxjs/operators';
import { DistributionGroup, DistributionGroupMember } from '@grid-failure-information-app/shared/models';
import { Store } from '@ngrx/store';

@Injectable()
export class DistributionGroupsEffects {
  getDistributionGroups$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.loadDistributionGroups),
      switchMap(() => {
        return this._apiClient.getDistributionGroups().pipe(
          map((distributionGroups: DistributionGroup[]) => {
            let groups = distributionGroupActions.loadDistributionGroupsSuccess({ payload: distributionGroups });
            groups.payload.sort((a, b) => a.name.localeCompare(b.name));
            return groups;
          }),
          catchError(error => of(distributionGroupActions.loadDistributionGroupsFail({ payload: error })))
        );
      })
    )
  );

  getDistributionGroupsDetail$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.loadDistributionGroupsDetail),
      switchMap(action => {
        return this._apiClient
          .getDistributionGroupDetails(action.payload)
          .pipe(
            map((distributionGroup: DistributionGroup) => {
              distributionGroup = {
                ...distributionGroup,
                emailSubjectView: distributionGroup.emailSubjectPublish,
                distributionTextView: distributionGroup.distributionTextPublish,
                emailSubjectViewShort: distributionGroup.emailSubjectPublishShort,
                distributionTextViewShort: distributionGroup.distributionTextPublishShort,
              };
              return distributionGroupActions.loadDistributionGroupsDetailSuccess({ payload: distributionGroup });
            }),
            catchError(error => of(distributionGroupActions.loadDistributionGroupsDetailFail({ payload: error })))
          )
      })
    )
  );

  saveDistributionGroup$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.saveDistributionGroup),
      map(action => action['payload']),
      exhaustMap((payload: DistributionGroup) => {
        return (payload.id ? this._apiClient.putDistributionGroup(payload.id, payload) : this._apiClient.postDistributionGroup(payload)).pipe(
          map((item: DistributionGroup) => {
            this._store.dispatch(distributionGroupActions.loadDistributionGroups());
            return distributionGroupActions.saveDistributionGroupSuccess({ payload: item });
          }),
          catchError(error => of(distributionGroupActions.saveDistributionGroupFail({ payload: error })))
        );
      })
    )
  );

  deleteDistributionGroup$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.deleteDistributionGroup),
      map(action => action['payload']),
      switchMap((id: string) => {
        return this._apiClient.deleteDistributionGroup(id).pipe(
          map(() => {
            this._store.dispatch(distributionGroupActions.loadDistributionGroups());
            return distributionGroupActions.deleteDistributionGroupSuccess();
          }),
          catchError(error => of(distributionGroupActions.deleteDistributionGroupFail({ payload: error })))
        );
      })
    )
  );

  getDistributionGroupMembers$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.loadDistributionGroupMembers),
      map(action => action['payload']),
      switchMap((id: string) => {
        return this._apiClient.getDistributionGroupMembers(id).pipe(
          map((groupMembers: DistributionGroupMember[]) => {
            let members = distributionGroupActions.loadDistributionGroupMembersSuccess({ payload: groupMembers });
            members.payload.sort((a, b) => a.name.localeCompare(b.name));
            return members;
          }),
          catchError(error => of(distributionGroupActions.loadDistributionGroupMembersFail({ payload: error })))
        );
      })
    )
  );

  deleteDistributionGroupMember$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.deleteDistributionGroupMember),
      switchMap(action => {
        return this._apiClient.deleteDistributionGroupMember(action.groupId, action.memberId).pipe(
          map(() => {
            this._store.dispatch(distributionGroupActions.loadDistributionGroupMembers({ payload: action.groupId }));
            return distributionGroupActions.deleteDistributionGroupMemberSuccess();
          }),
          catchError(error => of(distributionGroupActions.deleteDistributionGroupMemberFail({ payload: error })))
        );
      })
    )
  );

  getContacts$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.loadContacts),
      switchMap(action => {
        return this._apiClient.getContacts(action.searchText).pipe(
          map(item => distributionGroupActions.loadContactsSuccess({ payload: item })),
          catchError(error => of(distributionGroupActions.loadContactsFail({ payload: error })))
        );
      })
    )
  );

  createDistributionGroupMember$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.createDistributionGroupMember),
      exhaustMap(action => {
        return this._apiClient.postDistributionGroupMember(action.groupId, action.newMember).pipe(
          map(item => {
            this._store.dispatch(distributionGroupActions.loadDistributionGroupMembers({ payload: action.groupId }));
            return distributionGroupActions.createDistributionGroupMemberSuccess({ payload: item });
          }),
          catchError(error => of(distributionGroupActions.createDistributionGroupMemberFail({ payload: error })))
        );
      })
    )
  );

  putDistributionGroupMember$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.updateDistributionGroupMember),
      exhaustMap(action => {
        return this._apiClient.putDistributionGroupMember(action.groupId, action.memberId, action.member).pipe(
          map(item => {
            this._store.dispatch(distributionGroupActions.loadDistributionGroupMembers({ payload: action.groupId }));
            return distributionGroupActions.updateDistributionGroupMemberSuccess({ payload: item });
          }),
          catchError(error => of(distributionGroupActions.updateDistributionGroupMemberFail({ payload: error })))
        );
      })
    )
  );

  getDistributionTextPlaceholders$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.loadDistributionGroupTextPlaceholders),
      switchMap(() => {
        return this._apiClient.getDistributionGroupTextPlaceholders().pipe(
          map(item => distributionGroupActions.loadDistributionGroupTextPlaceholdersSuccess({ payload: item })),
          catchError(error => of(distributionGroupActions.loadDistributionGroupTextPlaceholdersFail({ payload: error })))
        );
      })
    )
  );

  exportContacts$: any = createEffect(() =>
    this._actions$.pipe(
      ofType(distributionGroupActions.exportContacts),
      switchMap(action => {
        return this._apiClient.exportContacts(action.groupId).pipe(
          map(item => distributionGroupActions.exportContactsSuccess({ payload: item })),
          catchError(error => of(distributionGroupActions.exportContactsFail({ payload: error })))
        );
      })
    )
  );

  constructor(private _actions$: Actions, private _apiClient: DistributionGroupApiClient, private _store: Store<any>) {}
}
