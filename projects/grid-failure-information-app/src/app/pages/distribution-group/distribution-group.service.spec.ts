/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DistributionGroup, DistributionGroupMember, Contact, DistributionGroupTextPlaceholder } from '@grid-failure-information-app/shared/models';
import { DistributionGroupService } from '@grid-failure-information-app/pages/distribution-group/distribution-group.service';

describe('DistributionGroupService', () => {
  beforeEach(() => {});

  it('should transform api response to list of models', () => {
    const response = [new DistributionGroup()];
    response[0].id = 'X';
    expect(DistributionGroupService.gridAdapter(response)[0].id).toBe('X');
  });

  it('should transform api response to model', () => {
    const response: any = { id: 'X' };
    const item = DistributionGroupService.itemAdapter(response);

    expect(item.id).toBe(response.id);
  });

  it('should transform api response to list of member models', () => {
    const response = [new DistributionGroupMember()];
    response[0].id = 'X';
    expect(DistributionGroupService.memberGridAdapter(response)[0].id).toBe('X');
  });

  it('should transform api response to list of contact models', () => {
    const response = { content: [new Contact()] };
    response.content[0].uuid = 'X';
    expect(DistributionGroupService.contactsPageAdapter(response)[0].uuid).toBe('X');
  });

  it('should transform api response to DistributionGroupTextPlaceholder model', () => {
    const response = new DistributionGroupTextPlaceholder();
    response.branch = 'X';
    expect(DistributionGroupService.placeholderAdapter(response).branch).toBe('X');
  });
});
