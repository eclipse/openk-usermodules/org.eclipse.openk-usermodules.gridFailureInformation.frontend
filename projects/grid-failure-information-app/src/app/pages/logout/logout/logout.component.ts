/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LogoutPageSandbox } from '@grid-failure-information-app/pages/logout/logout/logout.sandbox';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
})
export class LogoutPageComponent implements OnInit {
  @ViewChild('yesBtn', { static: true }) button: ElementRef;
  loggedOut = false;

  constructor(public logoutPageSandBox: LogoutPageSandbox) {}

  ngOnInit() {
    this.button.nativeElement.focus();
  }

  ngOnDestroy() {
    this.logoutPageSandBox.endSubscriptions();
  }
}
