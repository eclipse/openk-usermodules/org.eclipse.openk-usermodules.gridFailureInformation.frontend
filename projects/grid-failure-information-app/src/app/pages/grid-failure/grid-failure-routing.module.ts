/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GridFailureListComponent } from '@grid-failure-information-app/pages/grid-failure/grid-failure-list/grid-failure-list.component';
import { GridFailureDetailsComponent } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-details.component';
import { GridFailuresResolver } from '@grid-failure-information-app/pages/grid-failure/grid-failure.resolver';

const gridFailureRoutes: Routes = [
  {
    path: 'grid-failures/:gridFailureId',
    component: GridFailureDetailsComponent,
    resolve: {
      gridFailureDetails: GridFailuresResolver,
    },
  },
  {
    path: 'grid-failures',
    component: GridFailureListComponent,
    resolve: {
      gridFailuresTable: GridFailuresResolver,
    },
  },
  {
    path: 'grid-failures/new',
    component: GridFailureDetailsComponent,
    resolve: {
      gridFailureDetails: GridFailuresResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(gridFailureRoutes)],
  exports: [RouterModule],
})
export class GridFailureRoutingModule {}
