/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Req,
  Res,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { AppService, ExternalGridFailure, ExternalSettings } from './app.service';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/internal-sit')
  createPublicSITs(@Req() req: Request, @Res() res: Response): void {
    try {
      const body: ExternalGridFailure = req.body;
      if (!process.env.PASSWORD) {
        console.error('Web-Cache Password existiert nicht.');
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      } else if (!body.password) {
        console.error('Backend Password existiert nicht.');
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      } else if (process.env.PASSWORD !== body.password) {
        console.error('Password stimmt nicht überein.');
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      }

      this.appService.saveGridFailures(
        body,
        (isSuccesFullSave: boolean, message: string) => {
          res.statusMessage = message;
          isSuccesFullSave && res.sendStatus(HttpStatus.CREATED);
        },
      );
    } catch (error) {
      res.sendStatus(error.status);
    }
  }

  @Post('/fe-settings')
  createPublicSettings(@Req() req: Request, @Res() res: Response): void {
    try {
      const body: ExternalSettings = req.body;
      if (!process.env.PASSWORD) {
        console.error('Web-Cache Password existiert nicht.');
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      } else if (!body.password) {
        console.error('Backend Password existiert nicht.');
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      } else if (process.env.PASSWORD !== body.password) {
        console.error('Password stimmt nicht überein.');
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
      }
      this.appService.saveSettings(
        body,
        (isSuccesFullSave: boolean, message: string) => {
          res.statusMessage = message;
          isSuccesFullSave && res.sendStatus(HttpStatus.CREATED);
        },
      );
    } catch (error) {
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get('/public-settings')
  getSettings(@Res() res: Response): void {
    try {
      this.appService.getSettings((data: any) => {
        if (!!data && !!data.length) {
          res.status(HttpStatus.OK);
          res.send(data);
        } else {
          res.statusMessage = 'File is empty or file not found!';
          res.status(HttpStatus.NOT_FOUND);
          res.send(data);
        }
      });
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR);
      res.send([]);
    }
  }

  // Method signature without path parameter
  @Get('/public-sit')
  getSITsWithoutParam(@Res() res: Response): void {
    this.getSITsInternal(null, res);
  }

  // Method signature with path parameter
  @Get('/public-sit/:postcode')
  getSITsWithParam(@Param('postcode') postcode: string, @Res() res: Response): void {
    // Validate postcode
    const postcodePattern = /^[0-9]{4,5}$/;
    if (!postcodePattern.test(postcode)) {
      return;
    }
    this.getSITsInternal(postcode, res);
  }

  private getSITsInternal(postcode: string | null, res: Response) {
    try {
      this.appService.getSITs(postcode, (data: any) => {
        if (!!data) {
          res.status(HttpStatus.OK);
          res.send(data);
        } else {
          res.statusMessage = 'File is empty of file not found!';
          res.sendStatus(HttpStatus.NOT_FOUND);
        }
      });
    } catch (error) {
      res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
