/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { MapComponent } from '@grid-failure-information-map-app/app/app.component';
import { GridFailureSandbox } from '@grid-failure-information-map-app/app/grid-failure/grid-failure.sandbox';

describe('AppComponent', () => {
  let component: MapComponent;
  let sandbox: GridFailureSandbox;

  beforeEach(() => {
    sandbox = {
      initSandbox() {},
      unsubscribe() {},
      filterGridFailureMapList(value: string) {},

    } as any;
    component = new MapComponent(sandbox);
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it(`should have as title 'grid-failure-information-map-app'`, () => {
    expect(component.title).toEqual('grid-failure-information-map-app');
  });

  it('should unsubscribe subscriptions onDestroy', () => {
    const spy: any = spyOn(sandbox, 'unsubscribe');
    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });

  it('should init sandbox onInit', () => {
    const spy: any = spyOn(sandbox, 'initSandbox');
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should call sandbox.filterGridFailureMapList() if set postcode value', () => {
    const spy: jasmine.Spy  = spyOn(sandbox, 'filterGridFailureMapList');
    component.postcode = '007';
    expect(spy).toHaveBeenCalled();
  });
});
