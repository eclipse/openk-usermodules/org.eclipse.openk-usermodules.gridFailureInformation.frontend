/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, APP_INITIALIZER, ModuleWithProviders } from '@angular/core';
import { MapComponent } from '@grid-failure-information-map-app/app/app.component';
import { GridFailureInformationMapModule } from 'openk/grid-failure-information-map/src/public-api';
import { createCustomElement } from '@angular/elements';
import { GridFailureSandbox } from '@grid-failure-information-map-app/app/grid-failure/grid-failure.sandbox';
import { AppConfigService } from '@grid-failure-information-map-app/app/app-config.service';
import { APP_BASE_HREF, PlatformLocation } from '@angular/common';


/***
 * Get the BaseHref which is defined in the index.html ( <base href="/"> ).
 * The "base href" in the index.html can be set via ng nuild for different enviroments.
 *
 * */

export function getBaseHref(platformLocation: PlatformLocation): string {
  return platformLocation.getBaseHrefFromDOM();
}


export function getConfig(configservice: AppConfigService) {
  return () => configservice.getConfig();
}

const providers = [
  {
    provide: APP_INITIALIZER,
    useFactory: getConfig,
    deps: [AppConfigService],
    multi: true
  },
  GridFailureSandbox,
  AppConfigService,
  {
    provide: APP_BASE_HREF,
    useFactory: getBaseHref,
    deps: [PlatformLocation],
  },
];

@NgModule({
  declarations: [MapComponent],
  imports: [GridFailureInformationMapModule, BrowserModule],
    providers: providers
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    const ngElement = createCustomElement(MapComponent, {
      injector: this.injector,
    });
    customElements.define('openk-grid-failure-information-map-init', ngElement);
  }
}

@NgModule({})
export class MapModule {
  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: AppModule,
      providers: providers
    };
  }
}
